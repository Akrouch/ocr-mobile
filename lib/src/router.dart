import 'package:expense_ocr/src/interface/pages/receipt_data/receipt_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'interface/pages/photo/photo_page.dart';

class Router {
  static final _routes = {
    '/': () => PhotoPage(),
    ReceiptDataPage.route: () => ReceiptDataPage(),
  };

  static Route generateRoute(RouteSettings settings) {
    return CupertinoPageRoute(
      builder: (context) {
        if (_routes[settings.name] != null) {
          return _routes[settings.name]();
        }
        return Container();
      },
      settings: settings,
    );
  }
}
