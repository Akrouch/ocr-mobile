import 'package:bot_toast/bot_toast.dart';
import 'package:expense_ocr/src/interface/pages/photo/photo_page.dart';
import 'package:flutter/material.dart';
import 'package:expense_ocr/src/router.dart' as r;

import 'core/services/interface_service.dart';
import 'locator.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PhotoPage(),
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      navigatorKey: locator<InterfaceService>().navigationKey,
      debugShowCheckedModeBanner: false,
      onGenerateRoute: r.Router.generateRoute,
    );
  }
}
