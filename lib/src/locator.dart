import 'package:expense_ocr/src/core/provider/user_provider.dart';
import 'package:get_it/get_it.dart';

import 'core/services/api_service.dart';
import 'core/services/interface_service.dart';

GetIt locator = GetIt.instance;

setupLocator() {
  locator.registerLazySingleton(() => InterfaceService());
  locator.registerLazySingleton(() => ApiService());
  locator.registerLazySingleton(() => UserProvider());
}
