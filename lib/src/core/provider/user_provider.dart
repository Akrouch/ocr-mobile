import 'package:expense_ocr/src/core/services/api_service.dart';

import '../../locator.dart';

class UserProvider {
  final _apiService = locator<ApiService>();

  Future<bool> authenticate(Map<String, dynamic> data) async {
    return await _apiService
        .apiPost(route: ApiRoutes().auth, body: data)
        .then((response) {
      if (response.status == Status.success) {
        _apiService.generateToken(response.data['token']);
        return true;
      } else {
        print(response.data);
        return false;
      }
    });
  }
}
