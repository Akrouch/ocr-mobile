import 'package:document_scanner/document_scanner.dart';
import 'package:expense_ocr/src/interface/pages/photo/photo_page_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class PhotoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<PhotoPageModel>.reactive(
      viewModelBuilder: () => PhotoPageModel(),
      onModelReady: (model) => model.checkPermissions(),
      builder: (context, model, child) {
        print(model.isBusy);
        if (model.isBusy)
          return Center();
        else
          return Scaffold(
              appBar: AppBar(
                title: Text('loucura'),
              ),
              body: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 25),
                child: model.isCameraPermissionAccepted &&
                        model.isStoragePermissionAccepted
                    ? model.scannedDocument == null
                        ? !model.setCamera
                            ? GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: () {
                                  model.setCamera = true;
                                  model.notifyListeners();
                                },
                                child: Center(
                                  child: Icon(Icons.photo),
                                ),
                              )
                            : DocumentScanner(
                                onDocumentScanned: (ScannedImage scannedImage) {
                                  print("document : " +
                                      scannedImage.croppedImage);

                                  model.setScannedDocument(
                                    scannedImage.getScannedDocumentAsFile(),
                                  );
                                },
                              )
                        : SingleChildScrollView(
                            child: Column(
                              children: [
                                Image(
                                  image: FileImage(model.scannedDocument),
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {
                                        model.setScannedDocument(null);
                                      },
                                      child: Text('Tentar novamente'),
                                    ),
                                    ElevatedButton(
                                      onPressed: () {
                                        model.sendToApi();
                                      },
                                      child: Text('Enviar para a API'),
                                    )
                                  ],
                                )
                              ],
                            ),
                          )
                    : GestureDetector(
                        onTap: () {
                          model.checkPermissions();
                        },
                        child: Center(
                          child: Text(
                              'Permissões negadas. Clique para ativá-las.'),
                        ),
                      ),
              ));
      },
    );
  }
}
