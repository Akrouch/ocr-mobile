import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:expense_ocr/src/core/provider/user_provider.dart';
import 'package:expense_ocr/src/core/services/api_service.dart';
import 'package:expense_ocr/src/core/services/interface_service.dart';
import 'package:expense_ocr/src/interface/pages/receipt_data/receipt_data.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:stacked/stacked.dart';

import '../../../locator.dart';

class PhotoPageModel extends BaseViewModel {
  bool isCameraPermissionAccepted;
  bool isStoragePermissionAccepted;
  File scannedDocument;
  bool setCamera = false;
  final interfaceService = locator<InterfaceService>();
  final userProvider = locator<UserProvider>();

  void setScannedDocument(File file) {
    scannedDocument = file;
    notifyListeners();
  }

  void checkPermissions() async {
    setBusy(true);
    interfaceService.showLoader();
    PermissionStatus cameraPermission = await Permission.camera.request();
    PermissionStatus storagePermission = await Permission.storage.request();
    isCameraPermissionAccepted = cameraPermission.isGranted;
    isStoragePermissionAccepted = storagePermission.isGranted;
    interfaceService.closeLoader();
    notifyListeners();
    setBusy(false);
    print(isBusy);
  }

  void sendToApi() async {
    interfaceService.showLoader();
    bool authenticated = await userProvider.authenticate({
      "username": "34845",
      "password": "75411261657",
    });
    if (authenticated) {
      dynamic receiptReceived = await getReceiptInfo();
      print(receiptReceived);
      interfaceService.navigateTo(ReceiptDataPage.route,
          arguments: receiptReceived);
    }
    interfaceService.closeLoader();
  }

  Future<FormData> createFileData() async {
    var tmp = FormData.fromMap({
      'file': await MultipartFile.fromFile(scannedDocument.path),
    });
    return tmp;
  }

  Future<dynamic> getReceiptInfo() async {
    var formData = await createFileData();
    try {
      return await Dio()
          .post('${ApiService.baseUrl}${ApiRoutes().receipts}',
              data: formData,
              options: Options(
                headers: {
                  "content-type": ApiService.baseHeaders['content-type'],
                  "authorization": ApiService.baseHeaders['authorization'],
                },
              ))
          .then((response) {
        locator<InterfaceService>()
            .showSnackBar(message: 'Deu boa.', backgroundColor: Colors.green);
        return response.data;
      });
    } catch (e) {
      print(e);
      locator<InterfaceService>().showSnackBar(
          message: 'Algo errado aconteceu. Tente novamente.',
          backgroundColor: Colors.red);
      return 'Erro ao enviar recibo.';
    }
  }
}
