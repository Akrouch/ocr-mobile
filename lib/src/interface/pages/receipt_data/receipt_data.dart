import 'package:flutter/material.dart';

class ReceiptDataPage extends StatelessWidget {
  static const route = '/data';

  @override
  Widget build(BuildContext context) {
    print(ModalRoute.of(context).settings.arguments);
    final data =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 40),
        child: Column(
          children: [
            Text('Date: ${data['date']}'),
            Text('CNPJ: ${data['cnpj']}'),
            Text('Total: ${data['total']}'),
            Text('Método de pgto.: ${data['paymentMethod']}'),
            Text('Total pago: ${data['paidAmount']}'),
            Text('Troco: ${data['change']}'),
          ],
        ),
      ),
    );
  }
}
