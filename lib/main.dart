import 'package:expense_ocr/src/app.dart';
import 'package:expense_ocr/src/locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  setupLocator();
  runApp(App());
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}
